﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace advanced_calculator.Model
{
    internal class Arithmetic
    {
        private dynamic? equal;

        private List<dynamic> CreateList<T>(T a)
        {
            if (a == null)
                throw new ArgumentNullException(nameof(a));
            List<dynamic> l = new();
            if (a is IList)
            {                
                foreach (var item in (dynamic)a)
                    l.Add(item);
                if (!Functions.isNumber(l.First().GetType()))
                    throw new Exception("To nie jest wartość liczbowa");
            }
           // equal = l.First();
            return l;
        }

        public Arithmetic Add<T>(T a, T b)
        {
            return Add(a: new List<T> { a, b });
        }

        public Arithmetic Add<T>(T a)
        {
            equal = 0;
            foreach (var item in CreateList<T>(a))
                equal += item;
            return this;
        }

        public Arithmetic Sub<T>(T a, T b)
        {
            return Sub(a: new List<T> { a, b});
        }

        public Arithmetic Sub<T>(T a)
        {
            equal = 0;
            foreach (var item in CreateList<T>(a))
                equal -= item;
            return this;
        }

        public Arithmetic Mul<T>(T a, T b)
        {
            return Mul(a: new List<T> { a, b });
        }

        public Arithmetic Mul<T>(T a)
        {
            equal = 1;
            foreach (var item in CreateList<T>(a))
                equal *= item;
            return this;
        }

        public Arithmetic Div<T>(T a, T b)
        {
            return Mul(a: new List<T> { a, b });
        }

        public Arithmetic Div<T>(T a)
        {
            dynamic l = CreateList<T>(a);
            equal = l.First();
            for (int i=1;i< l.Count;i++)
            {
                if (l[i] == 0)
                    continue;
                equal /= l[i];
            }                
            return this;
        }

        public T GetEqual<T>()
        {
            return equal ?? throw new ArgumentNullException(nameof(equal));
        }
    }
}
