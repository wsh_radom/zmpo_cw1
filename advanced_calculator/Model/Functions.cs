﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace advanced_calculator.Model
{
    internal class Functions
    {
        internal static HashSet<Type> numbers = new HashSet<Type> { 
            typeof(int), typeof(float), typeof(double), typeof(byte), typeof(sbyte), typeof(short), typeof(ushort), typeof(decimal), typeof(long), typeof(ulong),
           typeof(Single), typeof(bool)       
        };

        public static bool isNumber(Type t)
        {
            return numbers.Contains(Nullable.GetUnderlyingType(t) ?? t);
        }
    }
}
