﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace advanced_calculator.View
{
    internal class Menu
    {
        readonly Dictionary<dynamic, List<string> > MenusOptions = new() { { 1, new() {"add", "Dodaj dwie liczby" } }, 
                                                        { 2,  new() { "addMulti", "Dodaj liczby" } }, 
                                                        { 3, new() { "sub", "Odjemij dwie liczby" } },
                                                        { 4, new() { "subMulti", "Odejmnij liczby" } },
                                                        { 6, new() { "mul", "Pomnóż dwie liczby" } },
                                                        { 7, new() { "mulMulti", "Pomnóż liczby" } },
                                                        { 8, new() { "div", "Podziel dwie liczby" } },
                                                        { 9, new() { "divMulti", "Podziel liczby" } },
                                                        { 5, new() { "end", "Zakończ program" } }
                                                    };

        void GetChoise()
        {
            dynamic v;
            for (; ; )
            {
                Console.Write("Wybrano: ");
                if (!MenusOptions.ContainsKey(v = Convert.ToInt64(Console.ReadLine())))
                    break;
                Console.WriteLine("Podałeś złą opcję menu. Spróbuj jeszcze raz!");
            }
            for (int i = 0;i< MenusOptions.Count;i++)
            {
                if (MenusOptions.Keys.ToList()[((int)v - 1)]==(i+1))
                {
                    try
                    {
                        Controller.Operations c = new Controller.Operations();
                        // MethodInfo
                        dynamic? mi = c.GetType().GetMethod(MenusOptions[i + 1].ElementAt(0)) ?? throw new NullReferenceException();
                        object [] t = { true };
                        _ = mi.Invoke(c, t);
                    }
                    catch (NullReferenceException ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
            }
            /*switch(MenusOptions.Keys.ToList()[((int)v - 1)])
            {
                case 1: Console.WriteLine(MenusOptions[1]); break;
                case 2: Console.WriteLine(MenusOptions[2]); break;
                case 3: Console.WriteLine(MenusOptions[3]); break;
                case 4: Console.WriteLine(MenusOptions[4]); break;
                case 5: Console.WriteLine(MenusOptions[5]); break;
                case 6: Console.WriteLine(MenusOptions[6]); break;
                case 7: Console.WriteLine(MenusOptions[7]); break;
                case 8: Console.WriteLine(MenusOptions[8]); break;
                case 9: Console.WriteLine(MenusOptions[9]); break;
               // case 2: Console.WriteLine(MenusOptions[2]); break;
            }*/

            //switch 
               // Console.WriteLine(MenusOptions.Keys.ToList()[((int)v-1)]);

        }

        public void CreateMenu()
        {
            int i = 1;
            foreach (var command in MenusOptions)
                Console.WriteLine((i++) + ". " + command.Value.ElementAt(1));
            GetChoise();
        }

        public static List<dynamic> GetNumbers<T>(bool multi=true) 
        {
            int i = 0;
            bool isInt = false;
            List<dynamic> list = new();
            for(; ; )
            {
                Console.Write("Podaj " + ++i + " liczbę: ");
                string? l = Console.ReadLine();
                
                if (l == null || l == "" )
                    break;
                if (l.Contains('.')) l.Replace('.', ',');
                if (i==1) {                    
                    double d = Convert.ToDouble(l);
                    isInt = Math.Abs(d % 1) <= (Double.Epsilon * 100);                   
                }
                var a = isInt ? Convert.ToInt32(l) : Convert.ToDouble(l);
                list.Add(a);
                if (!multi && i == 2) break;
            } 
            return list;
        }

        public static void ShowEquals(dynamic e)
        {
            Console.WriteLine("wynik działania to: " + e);
        }


    }
}
