﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace advanced_calculator.Controller
{
    internal class Operations
    {
        Model.Arithmetic arithmetic;
        public Operations()
        {
            arithmetic = new();
        }

        public void add(bool _) => addMulti(false);

        public void addMulti(bool multi=true)
        {
            dynamic e = arithmetic.Add(View.Menu.GetNumbers<double>(multi)).GetEqual<dynamic>();
            View.Menu.ShowEquals(e);
        }

        public void sub(bool _) => subMulti(false);

        public void subMulti(bool multi= true)
        {
            Console.WriteLine("Odejmowanie " + (multi ? "wiele" : "pary"));
        }

        public void mul(bool _) => mulMulti(false);

        public void mulMulti(bool multi= true)
        {
            Console.WriteLine("Mnożenie " + (multi ? "wiele" : "pary"));
        }

        public void div(bool _) => divMulti(false);

        public void divMulti(bool multi=true)
        {
            Console.WriteLine("Dzielenie " + (multi ? "wiele" : "pary"));
        }


        public void end(bool _)
        {
            Console.WriteLine("Koniec programu");
            System.Environment.Exit(0);
        }
    }
}
